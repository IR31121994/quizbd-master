<?php
session_start();

use App\Admin\Login;

require __DIR__ . '/vendor/autoload.php';
?>
<?php require "configuration.php";
require "database.php";
 ?>

<?php require "inc/header.php"; ?>

<div class="container-fluid text-center">

    <div class="row  bg-light pt-5">

        <div class="col-md-12">

            <div class="leaderboard pt-5 pb-3">
                <h1 class = "text-info pb-3">
                
                Leaderboard : QuizBD
            </h1>
            <?php 
            $uid = $_SESSION['user_id'];
            
      $sl=1;
      $data = "SELECT name, SUM(score) AS uscore FROM `leaderboard` GROUP BY `uid` ORDER BY `score` DESC";
      $result = $conn->query($data);
    
    if($result->num_rows > 0){
        echo ' <table class="table table-info  text-secondary table-responsive-md table-hover center" style="width:70%">
        <thead>
            <tr>
                <th >Rank</th>
                <th >Name</th>
                <th >Score</th>

            </tr>
        </thead>';
        
     
        while($row = $result->fetch_assoc()){
           
            echo '<tbody>
            <tr>
                <th>'.$sl++.'</th>
                <td>'.$row['name'].'</td>
                <td>'.$row['uscore'].'</td>
                
            </tr>
            

        </tbody>';
        }
        echo '</table>';
        
    }
    else{
        echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>No data found!</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>';
    }


?>

               
                    
                </table>
            </div>
            


        </div>

    </div>

</div>




<!-- category bikroy -->


<!-- category bikroy end -->


</body>

</html>