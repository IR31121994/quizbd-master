<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
require __DIR__ . '/vendor/autoload.php';
require "configuration.php";
require "database.php";
?>
<?php require "inc/header.php"; ?>
<div class="container-fluid text-center " style="margin-top:100px">
  <div class="row pt-5 pb-5 pt-5 bg-light">

    <div class="col-md-12 pt-5 pb-5">
      <?php

      if (isset($_GET['quizset'])) {
        $setid = $conn->escape_string($_GET['quizset']);
        $check = "SELECT id FROM results WHERE uid={$_SESSION['user_id']} AND qsetid= $setid";
        $checkResult = $conn->query($check);
        if($checkResult->num_rows > 0){
          echo"<h1 class = 'text-danger'>Already Attempt.</h1>";
        }
        else{
        $q = "select * from quizset where id=" . $setid . " limit 1";
        $quizes  = $conn->query($q);

        if ($quizes->num_rows > 0) {
          $quizes = $quizes->fetch_assoc();
          $timediff = strtotime($quizes['starttime']); //unix timestamp
          $exam_time = (date("Y-m-d H:i:s", $timediff));
          $nowtime = time(); //unix timestamp
          //$now_time = (date("Y-m-d H:i:s",$nowtime));

          //echo $nowtime - $timediff;
          $timeRemain = $nowtime - $timediff;
          $expiredTime = strtotime($quizes['duration']);
          $timeOver = $expiredTime - $nowtime;
          echo $timeOver;
         

          if ($timeOver < 0) {
            echo "<h1 class='text-danger'>Quiz time expired</h1>";
            echo "<h3>the time Over " .abs($timeOver) . "seconds</h3>";
          }
          if ($timeOver > 0) {
            $html = "";
            if ($timeRemain < 0) {
              echo "<h3 class='text-info' >" . $exam_time . "</h3>";
              echo "<h3 class='text-info'>Please wait " . abs($timeRemain) . " seconds for exam</h3> ";
            }
            if ($timeRemain > 0) {


              $quizids = $quizes['quizset'];
              //var_dump($quizids);
              $quizes1 = "select * from quizes where id in (" . $quizids . ")";
              $allsetQuizes = $conn->query($quizes1);
              $totalQuestions = $allsetQuizes->num_rows;
              echo "<h3 class='text-primary'>" . $quizes['setname'] . " (" . $totalQuestions . ")</h3>";
              //var_dump($allsetQuizes);
              $html = "<div class='card center' style='width:50%'>";
              
              $html .= "<form action='result.php' method='post'>";
              $html .= "<input type='hidden' name='setid' value='" . $setid . "'>";
              $html .= "<table id='tableContainer' class='table text-info table-borderless table-responsive-md table-hover center' >";
              $sl = 1;

              foreach ($allsetQuizes as $q) {
                // var_dump($q);
                // echo($q['id']);
                $html .= ' <tr class="fw-bolder text-info card-header ques"> <td colspan="2">Q' . $sl++ . '.      ' . $q['question'] . '</td></tr>
             <tr><td class="text-end"> <input type="radio" class="rdo" name="q[' . $q['id'] . ']" id="q' . $q['id'] . 'a" value="' . $q['op1'] . '"> </td><td class="text-start"> <label for="q' . $q['id'] . 'a">' . $q['op1'] . '</label></td></tr>
             <tr> <td class="text-end"> <input type="radio" class="rdo" name="q[' . $q['id'] . ']" id="q' . $q['id'] . 'b" value="' . $q['op2'] . '"> </td><td  class="text-start"> <label for="q' . $q['id'] . 'b">' . $q['op2'] . '</label></td></tr>
             <tr> <td class="text-end"> <input type="radio" class="rdo" name="q[' . $q['id'] . ']" id="q' . $q['id'] . 'c" value="' . $q['op3'] . '"> </td><td  class="text-start"> <label for="q' . $q['id'] . 'c">' . $q['op3'] . '</label></td></tr>
             <tr> <td class="text-end"> <input type="radio" class="rdo" name="q[' . $q['id'] . ']" id="q' . $q['id'] . 'd" value="' . $q['op4'] . '"> </td><td  class="text-start"> <label for="q' . $q['id'] . 'd">' . $q['op4'] . '</label></td></tr>
             
             <tr> <td colspan="2" style="border-bottom: 2px solid powderblue;">&nbsp;</td></tr>';
              }
              $html .= "</table>
          <input type='submit' class='btn btn-success mb-2' name='sbtn' id='sbtn' >
          </form>
          </div>";
            }
            echo $html;
          }
        }
      }
      }



      ?>




    </div>
  </div>
</div>


<?php require "inc/footer.php"; ?>

<script>
  $(document).ready(function() {

    // code to read selected table row cell data (values).

  });
</script>
</body>

</html>