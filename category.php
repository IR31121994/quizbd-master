<?php
session_start();
require __DIR__ . '/vendor/autoload.php';
require "database.php";
?>
<?php require "configuration.php"; ?>
<?php require "inc/header.php"; ?>
<div class="container-fluid text-center mt-5">
  <div class="row ">

    <div class="pt-5">
      <h1 class="text-info">ALL Topics</h1>
    </div>
    

 <?php
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    $startIndex = ($page - 1) * $items_per_page;


    if (isset($_GET['search'])) {
      $search = $_GET['search'];
      $products = "SELECT * FROM `topics` WHERE name LIKE '%$search%' order by created desc limit $startIndex, $items_per_page";
      $total_items = "SELECT count(*) as total FROM `topics` WHERE name LIKE '%$search%'";
    } else {
      $products = "SELECT * FROM `topics` WHERE 1 order by created desc limit $startIndex, $items_per_page";
      $total_items = "SELECT count(*) as total FROM `topics` WHERE 1";
    }
    // echo $products;

    $pro = $conn->query($products);
    $totalitems = $conn->query($total_items);
    $totalitems = $totalitems->fetch_assoc();
    $totalitems = $totalitems['total'];
    $totalpages = ceil($totalitems / $items_per_page);
    if ($pro->num_rows > 0) {

      while ($row = $pro->fetch_assoc()) {
        if ($row['icon'] == "") {
          $firstImage = "Broken-1.png";
        } else {
          $firstImage = explode(",", $row['icon'])[0];
        }

    ?>
        <div class="col-md-3 col-sm-6">
          <div class="product-grid p-2">
            <div class="topic-image p-2">

              <?php echo '<a href="quizset.php?topic=' . $row['id'] . '"><div class="item "><img src="assets/images/icons/' . $row['icon'] . '.png" title="' . $row['name'] . '" width="300" height="200"/><h4 class="text-center">' . $row['name'] . '</h4></div></a>'; ?>


            </div>

          </div>
        </div>
    <?php
      }
    }
    ?>
 

    


  </div>
</div>





<!-- pagination start -->
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="?page=1" tabindex="-1" aria-disabled="true">First</a>
    </li>
    <?php
    for ($i = 1; $i <= $totalpages; $i++) {
      if (abs($i - $page) < 3) {
        if ($i == $page) {
          echo '<li class="page-item active"><a class="page-link" href="category.php?page=' . $i . '">' . $page . '</a></li>';
        } else {
          echo '<li class="page-item"><a class="page-link" href="category.php?page=' . $i . '">' . $i . '</a></li>';
        }
      }
    }

    ?>
    <li class="page-item">
      <a class="page-link" href="?page=<?php echo $totalpages; ?>">Last</a>
    </li>
  </ul>
</nav>
<!-- pagination end -->

<div class="container-fluid text-center mt-5">
  <div class="row bg-light">
    <div class="pt-5">
      <h1 class="text-info">ALL CATEGORIES</h1>
    </div>
    <div class="col-md-12 mt-5">
      <div class="card pt-5 pb-5 bg-light">

        <?php
        $cat = "SELECT * FROM `categories` WHERE 1";
        $result = $conn->query($cat);
        if (isset($_SESSION['message'])) {
          echo '<div class="alert card alert-warning alert-dismissible fade show" role="alert">
    <strong>Message:!</strong> ' . $_SESSION['message'] . '.
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>';
          unset($_SESSION['message']);
        }
        if ($result->num_rows > 0) {
          echo '<div class="owl-carousel owl-theme">';
          while ($row = $result->fetch_assoc()) {
            echo '<a href="subcategory.php?cat=' . $row['id'] . '"><div class="item"><img src="assets/images/icons/' . $row['icon'] . '.png" title="' . $row['name'] . '" width="250" height="200"/><h4 class="text-center">' . $row['name'] . '</h4></div></a>';
          }
          echo '</div>';
        }
        ?>
      </div>
    </div>
  </div>
</div>


<?php require "inc/footer.php"; ?>
<script>
  function addbag(id) {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onload = function() {
      document.getElementById("cartitemtotal").innerHTML = this.responseText;
      console.log(this.responseText);
    }
    xmlhttp.open("GET", "ajax/addcart.php?id=" + id);
    xmlhttp.send();
  }
</script>
</body>

</html>